SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afiliacion`
--

CREATE TABLE IF NOT EXISTS `afiliacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `ipf` varchar(32) NOT NULL,
  `reg_cotizacion` int(11) DEFAULT NULL,
  `grupo_cotizacion` int(11) DEFAULT NULL,
  `tipo_contrato` int(11) DEFAULT NULL,
  `coef_temp_parcial` int(11) DEFAULT NULL,
  `fecha_alta_afi` date NOT NULL,
  `fecha_baja_afi` date DEFAULT NULL,
  `causa_baja_afi` int(11) DEFAULT NULL,
  `minus_alta_afi` int(11) DEFAULT NULL,
  `ccc` varchar(32) NOT NULL,
  `domicilio_cc` int(11) DEFAULT NULL,
  `cnae_2009` int(11) DEFAULT NULL,
  `num_trabajadores_cc` int(11) DEFAULT NULL,
  `fecha_alta_1cc` date DEFAULT NULL,
  `trl_cc` int(11) DEFAULT NULL,
  `colec_esp_cc` int(11) DEFAULT NULL,
  `id_empleador` int(11) DEFAULT NULL,
  `empleador_juri` varchar(1) DEFAULT NULL,
  `cccp` varchar(32) DEFAULT NULL,
  `provincia_ccp` int(11) DEFAULT NULL,
  `fecha_modificacion_1` date DEFAULT NULL,
  `tipo_contrato_1` int(11) DEFAULT NULL,
  `coef_temp_parcial_1` int(11) DEFAULT NULL,
  `fecha_modificacion_2` date DEFAULT NULL,
  `tipo_contrato_2` int(11) DEFAULT NULL,
  `coef_temp_parcial_2` int(11) DEFAULT NULL,
  `fecha_modificacion_grupo_1` date DEFAULT NULL,
  `grupo_cotizacion_1` int(11) DEFAULT NULL,
  `cnae93` varchar(16) DEFAULT NULL,
  `seta` tinyint(1) DEFAULT NULL,
  `tipo_relacion_otros` int(11) DEFAULT NULL,
  `fecha_efecto_alta` date DEFAULT NULL,
  `fecha_efecto_baja` date DEFAULT NULL,
  `episodio_vigente` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipf` (`ipf`,`fecha_alta_afi`,`ccc`),
  KEY `fecha_alta_afi` (`fecha_alta_afi`),
  KEY `reg_cotizacion` (`reg_cotizacion`),
  KEY `grupo_cotizacion` (`grupo_cotizacion`),
  KEY `tipo_contrato` (`tipo_contrato`),
  KEY `coef_temp_parcial` (`coef_temp_parcial`),
  KEY `causa_baja_afi` (`causa_baja_afi`),
  KEY `domicilio_cc` (`domicilio_cc`),
  KEY `cnae_2009` (`cnae_2009`),
  KEY `id_persona` (`id_persona`),
  KEY `fecha_baja_afi` (`fecha_baja_afi`),
  KEY `episodio_vigente` (`episodio_vigente`),
  KEY `trl_cc` (`trl_cc`),
  KEY `colec_esp_cc` (`colec_esp_cc`),
  KEY `id_empleador` (`id_empleador`),
  KEY `empleador_juri` (`empleador_juri`),
  KEY `provincia_ccp` (`provincia_ccp`),
  KEY `tipo_contrato_1` (`tipo_contrato_1`),
  KEY `coef_temp_parcial_1` (`coef_temp_parcial_1`),
  KEY `tipo_contrato_2` (`tipo_contrato_2`),
  KEY `coef_temp_parcial_2` (`coef_temp_parcial_2`),
  KEY `grupo_cotizacion_1` (`grupo_cotizacion_1`),
  KEY `cnae93` (`cnae93`),
  KEY `tipo_relacion_otros` (`tipo_relacion_otros`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Disparadores `afiliacion`
--
DROP TRIGGER IF EXISTS `update_id_persona`;
DELIMITER //
CREATE TRIGGER `update_id_persona` BEFORE INSERT ON `afiliacion`
 FOR EACH ROW SET NEW.id_persona=(SELECT id FROM persona WHERE ipf=NEW.ipf)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `convivientes`
--

CREATE TABLE IF NOT EXISTS `convivientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `ipf` varchar(32) NOT NULL,
  `fecha_nacimiento_1` date DEFAULT NULL,
  `sexo_1` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_2` date DEFAULT NULL,
  `sexo_2` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_3` date DEFAULT NULL,
  `sexo_3` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_4` date DEFAULT NULL,
  `sexo_4` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_5` date DEFAULT NULL,
  `sexo_5` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_6` date DEFAULT NULL,
  `sexo_6` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_7` date DEFAULT NULL,
  `sexo_7` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_8` date DEFAULT NULL,
  `sexo_8` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_9` date DEFAULT NULL,
  `sexo_9` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento_10` date DEFAULT NULL,
  `sexo_10` tinyint(4) DEFAULT NULL,
  `num_presonas` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipf` (`ipf`),
  UNIQUE KEY `id_persona` (`id_persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Disparadores `convivientes`
--
DROP TRIGGER IF EXISTS `update_id_persona_convivientes`;
DELIMITER //
CREATE TRIGGER `update_id_persona_convivientes` BEFORE INSERT ON `convivientes`
 FOR EACH ROW SET NEW.id_persona = ( SELECT id
FROM persona
WHERE ipf = NEW.ipf )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE IF NOT EXISTS `cotizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `ipf` varchar(32) NOT NULL,
  `ccc_2` varchar(32) NOT NULL,
  `ano_cotizacion` int(11) NOT NULL,
  `base_coti_mensual_enero` double NOT NULL,
  `base_coti_mensual_febrero` double NOT NULL,
  `base_coti_mensual_marzo` double NOT NULL,
  `base_coti_mensual_abril` double NOT NULL,
  `base_coti_mensual_mayo` double NOT NULL,
  `base_coti_mensual_junio` double NOT NULL,
  `base_coti_mensual_julio` double NOT NULL,
  `base_coti_mensual_agosto` double NOT NULL,
  `base_coti_mensual_septiembre` double NOT NULL,
  `base_coti_mensual_octubre` double NOT NULL,
  `base_coti_mensual_noviembre` double NOT NULL,
  `base_coti_mensual_diciembre` double NOT NULL,
  `base_coti_mensual_anual` double NOT NULL,
  `cotizacion_propia` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipf` (`ipf`,`ccc_2`,`ano_cotizacion`),
  KEY `id_persona` (`id_persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Disparadores `cotizacion`
--
DROP TRIGGER IF EXISTS `update_id_persona_cotizacion`;
DELIMITER //
CREATE TRIGGER `update_id_persona_cotizacion` BEFORE INSERT ON `cotizacion`
 FOR EACH ROW SET NEW.id_persona = ( SELECT id
FROM persona
WHERE ipf = NEW.ipf )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pension`
--

CREATE TABLE IF NOT EXISTS `pension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `ipf` varchar(32) NOT NULL,
  `ano_dato` int(11) NOT NULL,
  `id_presta` varchar(32) NOT NULL,
  `clase_presta` varchar(4) DEFAULT NULL,
  `sit_causante` int(11) DEFAULT NULL,
  `grado_inc` int(11) DEFAULT NULL,
  `fecha_minus` date DEFAULT NULL,
  `n_sovi` int(11) DEFAULT NULL,
  `clase_min` int(11) DEFAULT NULL,
  `reg_pension` int(11) DEFAULT NULL,
  `fecha_efect_eco` date DEFAULT NULL,
  `base_reg` double DEFAULT NULL,
  `pct_base_reg` double DEFAULT NULL,
  `anos_boni` int(11) DEFAULT NULL,
  `anos_coti_jubi` int(11) DEFAULT NULL,
  `imp_mensual_pension` double DEFAULT NULL,
  `imp_mensual_reval` double DEFAULT NULL,
  `imp_mensual_compl_min` double DEFAULT NULL,
  `imp_mensual_otros_compl` double DEFAULT NULL,
  `imp_mensual_total` double DEFAULT NULL,
  `situ_presta_causa` int(11) DEFAULT NULL,
  `fecha_situ_presta` date DEFAULT NULL,
  `provi_gest_presta` int(11) DEFAULT NULL,
  `prorrat_conv_internal` double DEFAULT NULL,
  `prorrat_divorcio` double DEFAULT NULL,
  `coef_reduc_total` double DEFAULT NULL,
  `tipo_situ_jubil` int(11) DEFAULT NULL,
  `coef_parcial_jubi` double DEFAULT NULL,
  `presta_vital` int(11) DEFAULT NULL,
  `concu_presta_ajena` int(11) DEFAULT NULL,
  `ano_nac_causa_preta` int(11) DEFAULT NULL,
  `pension_limit` int(11) DEFAULT NULL,
  `coef_reduc_lim_max` double DEFAULT NULL,
  `compat_jubi_trabajo` varchar(1) DEFAULT NULL,
  `fecha_ordi_jubil` date DEFAULT NULL,
  `period_coti_edad_ordi_jubil` int(11) DEFAULT NULL,
  `period_coti` int(11) DEFAULT NULL,
  `pct_anos_coti` double DEFAULT NULL,
  `import_mensual_total_compl_mater` double DEFAULT NULL,
  `pct_compl_mater` int(11) DEFAULT NULL,
  `coef_global_parci` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipf` (`ipf`,`ano_dato`,`id_presta`),
  KEY `id_persona` (`id_persona`),
  KEY `clase_presta` (`clase_presta`),
  KEY `grado_inc` (`grado_inc`),
  KEY `clase_min` (`clase_min`),
  KEY `reg_pension` (`reg_pension`),
  KEY `situ_presta_causa` (`situ_presta_causa`),
  KEY `provi_gest_presta` (`provi_gest_presta`),
  KEY `tipo_situ_jubil` (`tipo_situ_jubil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Disparadores `pension`
--
DROP TRIGGER IF EXISTS `update_id_persona_pension`;
DELIMITER //
CREATE TRIGGER `update_id_persona_pension` BEFORE INSERT ON `pension`
 FOR EACH ROW SET NEW.id_persona = ( SELECT id
FROM persona
WHERE ipf = NEW.ipf )
//
DELIMITER ;

--
-- Restricciones para tablas volcadas
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipf` varchar(15) NOT NULL,
  `tipo_documento` varchar(4) DEFAULT NULL,
  `numero_documento` varchar(10) NOT NULL,
  `codigo_multiplicidad` int(11) NOT NULL,
  `numero_tutelados` int(11) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `nacionalidad` varchar(3) DEFAULT NULL,
  `provincia_nacimiento` int(11) DEFAULT NULL,
  `provincia_primera_afiliacion` int(11) DEFAULT NULL,
  `domicilio_residencia_habitual` int(11) DEFAULT NULL,
  `fecha_fallecimiento` date DEFAULT NULL,
  `pais_nacimiento` varchar(3) DEFAULT NULL,
  `nivel_educativo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipf` (`ipf`),
  KEY `tipo_documento` (`tipo_documento`),
  KEY `nacionalidad` (`nacionalidad`),
  KEY `provincia_nacimiento` (`provincia_nacimiento`),
  KEY `provincia_primera_afiliacion` (`provincia_primera_afiliacion`),
  KEY `domicilio_residencia_habitual` (`domicilio_residencia_habitual`),
  KEY `pais_nacimiento` (`pais_nacimiento`),
  KEY `nivel_educativo` (`nivel_educativo`),
  KEY `sexo` (`sexo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `afiliacion`
--
ALTER TABLE `afiliacion`
  ADD CONSTRAINT `afiliacion_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_2` FOREIGN KEY (`reg_cotizacion`) REFERENCES `regimen_cotizacion` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_3` FOREIGN KEY (`grupo_cotizacion`) REFERENCES `grupo_cotizacion` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_4` FOREIGN KEY (`tipo_contrato`) REFERENCES `tipo_contrato` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_5` FOREIGN KEY (`coef_temp_parcial`) REFERENCES `coef_tiempo_parcial` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_6` FOREIGN KEY (`causa_baja_afi`) REFERENCES `tipo_causa_baja_afi` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_7` FOREIGN KEY (`domicilio_cc`) REFERENCES `municipio` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_8` FOREIGN KEY (`cnae_2009`) REFERENCES `cnae2009_grupo` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_9` FOREIGN KEY (`trl_cc`) REFERENCES `tipo_relacion_laboral` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_10` FOREIGN KEY (`colec_esp_cc`) REFERENCES `colectivo_especial_cc` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_11` FOREIGN KEY (`id_empleador`) REFERENCES `tipo_identificador_empleador` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_12` FOREIGN KEY (`empleador_juri`) REFERENCES `tipo_entidad_juridica` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_13` FOREIGN KEY (`provincia_ccp`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_14` FOREIGN KEY (`tipo_contrato_1`) REFERENCES `tipo_contrato` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_15` FOREIGN KEY (`coef_temp_parcial_1`) REFERENCES `coef_tiempo_parcial` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_16` FOREIGN KEY (`tipo_contrato_2`) REFERENCES `tipo_contrato` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_17` FOREIGN KEY (`coef_temp_parcial_2`) REFERENCES `coef_tiempo_parcial` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_18` FOREIGN KEY (`grupo_cotizacion_1`) REFERENCES `grupo_cotizacion` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_19` FOREIGN KEY (`cnae93`) REFERENCES `cnae93` (`id`),
  ADD CONSTRAINT `afiliacion_ibfk_20` FOREIGN KEY (`tipo_relacion_otros`) REFERENCES `tipo_rel_entidades_autonomos` (`id`);

--
-- Filtros para la tabla `convivientes`
--
ALTER TABLE `convivientes`
  ADD CONSTRAINT `convivientes_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD CONSTRAINT `cotizacion_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `pension`
--
ALTER TABLE `pension`
  ADD CONSTRAINT `pension_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `pension_ibfk_2` FOREIGN KEY (`grado_inc`) REFERENCES `grado_incapacidad` (`id`),
  ADD CONSTRAINT `pension_ibfk_3` FOREIGN KEY (`clase_min`) REFERENCES `clase_minimo` (`id`),
  ADD CONSTRAINT `pension_ibfk_4` FOREIGN KEY (`reg_pension`) REFERENCES `regimen_pension` (`id`),
  ADD CONSTRAINT `pension_ibfk_5` FOREIGN KEY (`situ_presta_causa`) REFERENCES `situacion_prestacion` (`id`),
  ADD CONSTRAINT `pension_ibfk_6` FOREIGN KEY (`provi_gest_presta`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `pension_ibfk_7` FOREIGN KEY (`tipo_situ_jubil`) REFERENCES `situacion_jubilacion` (`id`),
  ADD CONSTRAINT `pension_ibfk_8` FOREIGN KEY (`clase_presta`) REFERENCES `clase_prestacion` (`id`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`tipo_documento`) REFERENCES `tipo_documento` (`id`),
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`nacionalidad`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`provincia_nacimiento`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `persona_ibfk_4` FOREIGN KEY (`provincia_primera_afiliacion`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `persona_ibfk_5` FOREIGN KEY (`domicilio_residencia_habitual`) REFERENCES `municipio` (`id`),
  ADD CONSTRAINT `persona_ibfk_6` FOREIGN KEY (`pais_nacimiento`) REFERENCES `pais` (`id`),
  ADD CONSTRAINT `persona_ibfk_7` FOREIGN KEY (`nivel_educativo`) REFERENCES `nivel_educativo` (`id`),
  ADD CONSTRAINT `persona_ibfk_8` FOREIGN KEY (`sexo`) REFERENCES `sexo` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
